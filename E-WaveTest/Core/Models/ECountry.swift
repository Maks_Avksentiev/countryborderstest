//
//  ECountry.swift
//  E-WaveTest
//
//  Created by Maksim Avksentev on 1/28/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import Foundation

struct ECountry {
    
    enum CodingKeys: String, CodingKey {
    
        case name
        case capital
        case alpha3Code
        case borders
    }
    
    public let name: String
    public let capital: String
    public let code: String
    public let borders: [String]
}

//MARK: - Additional Init
extension ECountry: Decodable {
    
    init(_ country: Country) {
        
        self.init(name: country.name ?? "", capital: country.capital ?? "", code: country.code ?? "", borders: country.borders ?? [String]())
    }
    
    //MARK: - Decodable
    init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decode(String.self, forKey: .name)
        self.capital = try container.decode(String.self, forKey: .capital)
        self.code = try container.decode(String.self, forKey: .alpha3Code)
        self.borders = try container.decode([String].self, forKey: .borders)
    }
}
