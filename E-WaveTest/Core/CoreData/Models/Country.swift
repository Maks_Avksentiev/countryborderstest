//
//  Country.swift
//  E-WaveTest
//
//  Created by Maksim Avksentev on 1/28/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import Foundation
import CoreData

extension Country {
    
    class func find(inContext context: NSManagedObjectContext, with code: String) -> Country? {

        guard let country: Country = ((CoreData.manager.read(context, parametrs: ["code": code])) as? [Country])?.first else {
            
            return nil
        }

        return country
    }
    
    class func findAll(inContext context: NSManagedObjectContext) -> [Country]? {
        
        let countries: [Country] = CoreData.manager.read(context)
        return countries
    }
}
