//
//  CRUDProtocol.swift
//  E-WaveTest
//
//  Created by Maksim Avksentev on 1/28/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import Foundation
import CoreData

protocol CRUDProtocol {
    
    func create<T: NSManagedObject>(_ context: NSManagedObjectContext, parametrs: [String: Any?]) -> T?
    
    func read<T: NSManagedObject>(_ context: NSManagedObjectContext, parametrs: [String: Any?]?, sortByKey: String?, ascending: Bool) -> [T]
    
    func createOrUpdate<T: NSManagedObject>(_ context: NSManagedObjectContext, primary: [String: Any?], secondary: [String: Any?]) -> T?
    
    func delete<T: NSManagedObject>(_ context: NSManagedObjectContext, parametrs: [String: Any?]) -> T?
}
