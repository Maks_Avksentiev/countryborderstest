//
//  HTTPCommunication.swift
//  E-WaveTest
//
//  Created by Maksim Avksentev on 1/28/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import Foundation

class HTTPCommunication {
    
    struct Methods {
        static let getAll = URL(string: "https://restcountries.eu/rest/v2/all")!
    }
    
    class func getAll(_ completionHandler: @escaping ((Data?) -> Void)) {
    
        self.retrieveURL(Methods.getAll) { (data) in
            completionHandler(data)
        }
    }
}

//MARK: - Main
extension HTTPCommunication {
    
    class fileprivate func retrieveURL(_ url: URL, completionHandler: @escaping ((Data?) -> Void)) {
        
        let session = URLSession(configuration: .default)
        
        let task = session.dataTask(with: URLRequest(url: url)) { (data, _, _) in
            
            guard let data = data else {
                
                completionHandler(nil)
                return
            }
            
            completionHandler(data)
        }
        
        task.resume()
    }
}
