//
//  TableDataSource.swift
//  E-WaveTest
//
//  Created by Maksim Avksentev on 1/28/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit

public typealias TableItemSelectionHandlerType = (IndexPath) -> Void

open class TableDataSource<Provider: TableDataProvider, Cell: UITableViewCell>: NSObject, UITableViewDataSource, UITableViewDelegate where Cell: ConfigurableCell, Provider.T == Cell.T {
    
    // MARK: - Delegates
    public var tableItemSelectionHandler: TableItemSelectionHandlerType?
    
    // MARK: - Private Properties
    let provider: Provider
    let tableView: UITableView
    
    // MARK: - Initialize
    init(tableView: UITableView, provider: Provider) {
        
        self.tableView = tableView
        self.provider = provider
        
        super.init()
        
        self.setUp()
    }
    
    fileprivate func setUp() {
        
        DispatchQueue.main.async {
            
            self.tableView.register(UINib(nibName: Cell.reuseIdentifier, bundle: Bundle.main) , forCellReuseIdentifier: Cell.reuseIdentifier)
            self.tableView.dataSource = self
            self.tableView.delegate = self
        }
    }
    
    // MARK: - UITableViewDataSource
    public func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.provider.numberOfSections()
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.provider.numberOfItems(in: section)
    }

    open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Cell.reuseIdentifier, for: indexPath) as? Cell else {
            
            return UITableViewCell()
        }
        
        if let item = self.provider.item(at: indexPath) {
            cell.configure(item, at: indexPath)
        }
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableItemSelectionHandler?(indexPath)
    }
}
