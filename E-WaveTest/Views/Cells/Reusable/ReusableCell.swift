//
//  ReusableCell.swift
//  E-WaveTest
//
//  Created by Maksim Avksentev on 1/28/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit

public protocol ReusableCell {
    
    static var reuseIdentifier: String { get }
}

public extension ReusableCell {
    
    static var reuseIdentifier: String {
        
        return String(describing: self)
    }
}
