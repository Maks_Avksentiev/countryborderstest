//
//  ConfigurableCell.swift
//  E-WaveTest
//
//  Created by Maksim Avksentev on 1/28/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit

public protocol ConfigurableCell: ReusableCell {
    
    associatedtype T
    
    func configure(_ item: T, at indexPath: IndexPath)
}
