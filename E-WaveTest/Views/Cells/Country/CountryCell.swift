//
//  CountryCell.swift
//  E-WaveTest
//
//  Created by Maksim Avksentev on 1/28/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit

class CountryCell: UITableViewCell {
    
}

//MARK: - ConfigurableCell
extension CountryCell: ConfigurableCell {
    
    func configure(_ item: ECountry, at indexPath: IndexPath) {
        
        self.textLabel?.text = item.name
        self.detailTextLabel?.text = item.capital
    }
}
