//
//  MainController.swift
//  E-WaveTest
//
//  Created by Maksim Avksentev on 1/28/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit

class MainController: BaseTableController {

    //MARK: - LifeCycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.countriesDataSource = self.setUpDataSource()
        self.requestAll()
    }
}
