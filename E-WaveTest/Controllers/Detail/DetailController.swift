//
//  DetailController.swift
//  E-WaveTest
//
//  Created by Maksim Avksentev on 1/28/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit

class DetailController: BaseTableController {
    
    var viewModel: ECountry?
    var selectedIndexPath: IndexPath? = nil
    
    //MARK: - Init
    init(dataSource: CountriesDataSource?, selectedIndex: IndexPath?) {
        
        super.init(nibName: String(describing: type(of: self)), bundle: Bundle.main)
        
        self.selectedIndexPath = selectedIndex
        self.countriesDataSource = dataSource
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
    }
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.tableView.allowsSelection = false
        self.configure()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        self.setTitle()
        self.neighborsChecker()
    }
}

//MARK: - Private
extension DetailController {
    
    fileprivate func configure() {
        
        guard let selectedIndexPath = self.selectedIndexPath else {
            
            return
        }
        
        self.viewModel = self.countriesDataSource?.item(at: selectedIndexPath)
        
        let numberOfItems = self.countriesDataSource?.provider.numberOfItems(in: selectedIndexPath.section) ?? 0
        
        let viewModels: [ECountry] = Array(0..<numberOfItems)
            .compactMap {
                
                if let item = self.countriesDataSource?.item(at: IndexPath(row: $0, section: selectedIndexPath.section)) {
                    
                    let value = self.viewModel?.borders.contains(item.code) ?? false
                    return value ? item : nil
                }
                
                return nil
        }
        
        self.countriesDataSource = CountriesDataSource(tableView: self.tableView, array: [viewModels])
        
    }
    
    fileprivate func setTitle() {
        
        self.title = self.viewModel?.name
    }
    
    fileprivate func neighborsChecker() {
        
        guard self.viewModel?.borders.count != 0 else {
            
            let alertController = UIAlertController(title: "Attention", message: "This country hasn`t neighbors", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { [weak self] (action) in
                
                self?.navigationController?.popViewController(animated: true)
            }))
            
            self.present(alertController, animated: true, completion: nil)
            
            return
        }
        
        self.tableView.reloadData()
    }
}
