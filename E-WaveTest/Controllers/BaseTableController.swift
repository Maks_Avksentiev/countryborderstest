//
//  BaseTableController.swift
//  E-WaveTest
//
//  Created by Maksim Avksentev on 1/28/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit

class BaseTableController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var countriesDataSource: CountriesDataSource?
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
     
        super.viewDidLoad()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        super.viewDidDisappear(animated)
        
        self.activityIndicator?.stopAnimating()
    }
}

//MARK: - Data Source
class CountriesDataSource: TableArrayDataSource<ECountry, CountryCell> {}

extension BaseTableController {
    
    func setUpDataSource() -> CountriesDataSource {
        
        let databaseModels: [Country]? = Country.findAll(inContext: CoreData.manager.currentThreadContext())
        let viewModels: [ECountry] = databaseModels?.map({
                ECountry($0)
            }) ?? [ECountry]()
        
        let dataSource = CountriesDataSource(tableView: self.tableView, array: [viewModels])
        dataSource.tableItemSelectionHandler = { [weak self] indexPath in
            
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.tableView.deselectRow(at: indexPath, animated: true)
            let controller = DetailController(dataSource: strongSelf.countriesDataSource, selectedIndex: indexPath)
            strongSelf.navigationController?.pushViewController(controller, animated: true)
        }
        
        viewModels.count != 0 ? () : (self.activityIndicator?.startAnimating())
        
        return dataSource
    }
    
    func requestAll() {
        
        DispatchQueue.global(qos: .userInteractive).async {
            
            HTTPCommunication.getAll { [weak self] (data) in
                
                guard let data = data, let _self = self else {
                    
                    self?.activityIndicator?.stopAnimating()
                    
                    return
                }
                
                let results: [ECountry] = (try? JSONDecoder().decode([ECountry].self, from: data)) ?? [ECountry]()
                
                let dataSource = CountriesDataSource(tableView: _self.tableView, array: [results])
                dataSource.tableItemSelectionHandler = { [weak self] indexPath in
                    
                    guard let strongSelf = self else {
                        return
                    }
                    
                    strongSelf.tableView.deselectRow(at: indexPath, animated: true)
                    let controller = DetailController(dataSource: strongSelf.countriesDataSource, selectedIndex: indexPath)
                    self?.navigationController?.pushViewController(controller, animated: true)
                }
                
                _self.countriesDataSource = dataSource
              
                DispatchQueue.main.async {
                    _self.activityIndicator?.stopAnimating()
                    _self.tableView.reloadData()
                }
                
                DispatchQueue.global(qos: .userInteractive).async { [weak self] in
                    
                    if self?.countriesDataSource?.provider.items.first?.count != dataSource.provider.items.first?.count {
                        
                        results.forEach({
                            
                            if $0.code != "" {
                                let _: Country? = CoreData.manager.createOrUpdate(CoreData.manager.currentThreadContext(), primary: ["code": $0.code], secondary: ["capital": $0.capital, "borders": $0.borders, "name": $0.name])
                            }
                        })
                    }
                }
            }
        }
    }
}
