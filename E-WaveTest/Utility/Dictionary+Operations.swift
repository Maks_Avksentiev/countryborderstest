//
//  Dictionary+Operations.swift
//  E-WaveTest
//
//  Created by Maksim Avksentev on 1/28/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import Foundation

func += <KeyType, ValueType> (left: inout Dictionary<KeyType, ValueType>,
                              right: Dictionary<KeyType, ValueType>) {
    
    for (k, v) in right {
        left.updateValue(v, forKey: k)
    }
}

func + <KeyType, ValueType>(left: Dictionary<KeyType, ValueType>,
                            right: Dictionary<KeyType, ValueType>) -> Dictionary<KeyType, ValueType> {
    
    var map = Dictionary<KeyType, ValueType>()
    
    for (k, v) in left {
        map[k] = v
    }
    
    for (k, v) in right {
        map[k] = v
    }
    
    return map
}
